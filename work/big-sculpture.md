---
layout: layouts/post.njk
tags:
 - work
 - "2019"
 - print
title: Where Does Power Lie?
type: Print
year: "2019"
featured_image: "/img/61_5-warnick-greece-show.jpg"
materials: Altered Postcard
description: A vintage postcard altered with custom stamp.
support_images:
 - "/img/61_1-warnick-greece-show.jpg"
 - "/img/61_2-warnick-greece-show.jpg"
 - "/img/61_3-warnick-greece-show.jpg"
 - "/img/61_4-warnick-greece-show.jpg"
 - "/img/61_5-warnick-greece-show.jpg"
eleventyNavigation:
  key: Where Does Power Lie?
---